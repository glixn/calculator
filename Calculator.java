import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Calculator {

    private static class Evaluator {
        private String expr;

        public Evaluator(String expr) {
            this.expr = expr;
        }

        public String getResult() {
            if (expr.isEmpty())
                return "";
            else
                return Integer.toString(evaluate());
        }

        private int evaluate() {
            expr = normalize(expr);
            Stack<Integer> vals = new Stack<>();
            Stack<Character> ops = new Stack<>();
            char[] tokens = expr.toCharArray();

            for (int i = 0; i < tokens.length; i++) {
                if (Character.isDigit(tokens[i])) {
                    StringBuilder numBuilder = new StringBuilder();
                    while (i<tokens.length && Character.isDigit(tokens[i])) {
                        numBuilder.append(tokens[i++]);
                    }
                    vals.push(Integer.parseInt(numBuilder.toString()));
                    i--;
                }
                else if (isOperator(tokens[i])) {
                    while(!ops.empty() && ops.peek() != '(' && hasPrecedence(ops.peek(),tokens[i])) {
                        operate(vals,ops);
                    }
                    ops.push(tokens[i]);
                }
                else if (tokens[i] == '(') {
                    ops.push(tokens[i]);
                }
                else if (tokens[i] == ')') {
                    while(ops.peek() != '(') {
                        operate(vals,ops);
                    }
                    ops.pop();
                }
            }

            while (!ops.empty()) {
                operate(vals,ops);
            }
            return vals.empty() ? 0 : vals.peek();
        }


        private void operate(Stack<Integer> vals, Stack<Character> ops) {
            if (ops.peek() == 'u') {
                vals.push(operateMinus(vals.pop()));
                ops.pop();
            }
            else {
                vals.push(operateBinary(ops.pop(), vals.pop(), vals.pop()));
            }
        }

        private int operateMinus(int op1) {
            return -op1;
        }

        private int operateBinary(Character operator, int op1, int op2) {
            switch (operator) {
                case '+':
                    return op2+op1;
                case '-':
                    return op2-op1;
                case '*':
                    return op2*op1;
                case '/':
                    return op2/op1;
                default:
                    return 0;
            }
        }

        private String normalize(String expr) {
            StringBuilder sb = new StringBuilder();
            List<Character> normalizedTokens = new ArrayList<Character>();
            char[] tokens = expr.toCharArray();
            char curr, prev;

            for (int i = 0; i<tokens.length; i++) {
                if (tokens[i] != ' ')
                    normalizedTokens.add(tokens[i]);
            }

            if (normalizedTokens.get(0) == '-') {
                sb.append('u');
            }
            else {
                sb.append(normalizedTokens.get(0));
            }

            for (int i = 1; i<normalizedTokens.size(); i++) {
                curr = normalizedTokens.get(i);
                prev = normalizedTokens.get(i-1);
                if (curr == '-' && (isOperator(prev) || prev == '('))
                    sb.append('u');
                else
                    sb.append(curr);
            }

            return sb.toString();
        }

        private boolean isOperator(char token) {
            return token == '+' || token == '-' || token == '*' || token == '/' || token == 'u';
        }

        private boolean hasPrecedence(char op1, char op2) {
            if (op2 == 'u' ||
                    ((op1 == '+' || op1 == '-') && (op2 == '*' || op2 == '/'))) {
                return false;
            }
            else {
                return true;
            }
        }
    }

    public static void main(String[] argv) {
        PrintStream out;
        String line;

        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            out = System.out;

            if (argv.length == 0) {
                System.out.println("Type an arithmetic expression:");
                line = in.readLine();
                Evaluator eval = new Evaluator(line);
                out.println(eval.getResult());
            }
            else {
                in = new BufferedReader(new FileReader(argv[0]));
                if (argv.length > 1) {
                    out = new PrintStream(new FileOutputStream(argv[1]));
                }
                while ((line = in.readLine()) != null) {
                    Evaluator eval = new Evaluator(line);
                    out.println(eval.getResult());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
